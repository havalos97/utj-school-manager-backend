## UTJ-SchoolManager

### To create the .env file, use the .env.example file included in the repo

#### Steps:
 1. cp .env.example .env
 2. Configure all .env desired data
 3. npm install
 4. Create a database and call it whatever you want (this will be the name you use in the .env file)

