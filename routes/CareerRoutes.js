const express = require('express');
const router = express.Router();
const CareerController = require('../controllers/CareerController.js');

// CREATE
router.post('/', (request, response) => {
	return CareerController.createCareer(request, response);
});

// READ
router.get('/', (request, response) => {
	return CareerController.getAllCareers(request, response);
});

// READ
router.get('/:id', (request, response) => {
	return CareerController.findCareer(request, response);
});

// UPDATE
router.put('/:id', (request, response) => {
	return CareerController.updateCareer(request, response);
});

// UPDATE
router.patch('/:id', (request, response) => {
	return CareerController.updateCareer(request, response);
});

// DELETE
router.delete('/:id', (request, response) => {
	return CareerController.deleteCareer(request, response);
});

module.exports = router;
