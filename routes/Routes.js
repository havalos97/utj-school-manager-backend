const UserRoutes = require('./UserRoutes.js');
const CareerRoutes = require('./CareerRoutes.js');

module.exports = {
	UserRoutes,
	CareerRoutes,
}
