const db = require('../models');

async function getAllCareers(request, response) {
    try {
        const userList = await db.career.findAll();
        return response.status(200).json({
            status: 'OK',
            data: userList,
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function createCareer(request, response) {
    const newCareer = request.body;
    try {
        const newCareerData = await db.career.create(newCareer);
        return response.status(201).json({
            status: 'Created',
            data: newCareerData,
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function findCareer(request, response) {
    try {
        const careerData = await db.career.findByPk(request.params.id);
        if (careerData) {
            return response.status(200).json({
                status: 'FOUND',
                data: careerData,
            });
        }
        return response.status(200).json({
            status: 'NOT FOUND',
            data: {},
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function deleteCareer(request, response) {
    try {
        let careerData = await db.career.findByPk(request.params.id);
        if (careerData) {
            await db.career.destroy({
                where: {
                    id: request.params.id,
                }
            })
            return response.status(200).json({
                status: 'DELETED',
                data: careerData,
            });
        }
        return response.status(200).json({
            status: 'NOT FOUND',
            data: {},
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function updateCareer(request, response) {
    try {
        let careerData = await db.career.findByPk(request.params.id);
        if (careerData) {
            await db.career.update(request.body, {
                where: {
                    id: request.params.id
                }
            });
            let newCareerData = await db.career.findByPk(request.params.id);

            return response.status(200).json({
                status: 'UPDATED',
                data: newCareerData,
            });
        }
        return response.status(200).json({
            status: 'NOT FOUND',
            data: {},
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

module.exports = {
    getAllCareers,
    createCareer,
    findCareer,
    deleteCareer,
    updateCareer,
}
