const db = require('../models');

async function getAllUsers(request, response) {
	try {
		const userList = await db.user.findAll();
		return response.status(200).json({
			status: 'OK',
			data: userList,
		});
	} catch (e) {
		console.error(e)
	}
	return response.status(500).json({
		status: 'Internal Server Error',
		errorMessage: 'Check output log for more details.',
	});
}

async function createUser(request, response) {
	const newUser = request.body;
	try {
		const newUserData = await db.user.create(newUser);
		return response.status(201).json({
			status: 'Created',
			data: newUserData,
		});
	} catch (e) {
		console.error(e)
	}
	return response.status(500).json({
		status: 'Internal Server Error',
		errorMessage: 'Check output log for more details.',
	});
}

async function findUser(request, response) {
	try {
		const userData = await db.user.findByPk(request.params.id);
		if (userData) {
			return response.status(200).json({
				status: 'FOUND',
				data: userData,
			});
		}
		return response.status(200).json({
			status: 'NOT FOUND',
			data: {},
		});
	} catch (e) {
		console.error(e)
	}
	return response.status(500).json({
		status: 'Internal Server Error',
		errorMessage: 'Check output log for more details.',
	});
}

async function deleteUser(request, response) {
	try {
		let userData = await db.user.findByPk(request.params.id);
		if (userData) {
			await db.user.destroy({
				where: {
					id: request.params.id,
				}
			})
			return response.status(200).json({
				status: 'DELETED',
				data: userData,
			});
		}
		return response.status(200).json({
			status: 'NOT FOUND',
			data: {},
		});
	} catch (e) {
		console.error(e)
	}
	return response.status(500).json({
		status: 'Internal Server Error',
		errorMessage: 'Check output log for more details.',
	});
}

async function updateUser(request, response) {
	try {
		let userData = await db.user.findByPk(request.params.id);
		if (userData) {
			await db.user.update(request.body, {
				where: {
					id: request.params.id
				}
			});
			let newUserData = await db.user.findByPk(request.params.id);

			return response.status(200).json({
				status: 'UPDATED',
				data: newUserData,
			});
		}
		return response.status(200).json({
			status: 'NOT FOUND',
			data: {},
		});
	} catch (e) {
		console.error(e)
	}
	return response.status(500).json({
		status: 'Internal Server Error',
		errorMessage: 'Check output log for more details.',
	});
}

module.exports = {
	getAllUsers,
	createUser,
	findUser,
	deleteUser,
	updateUser,
}