module.exports = (sequelize, DataTypes) => {
	const UserModel = sequelize.define('user', {
		username: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		firstName: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		lastName: {
			type: DataTypes.STRING,
		},
	});

	UserModel.associate = (models) => {
		UserModel.belongsTo(models.career, {
			foreignKey: {
				allowNull: false,
			}
		});
	};

	return UserModel;
};
