module.exports = (sequelize, DataTypes) => {
    const CareerModel = sequelize.define('career', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    });

    CareerModel.associate = (models) => {
        CareerModel.hasMany(models.user);
    };

    return CareerModel;
};
