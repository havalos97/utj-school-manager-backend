const express = require('express');
const app = express();
const db = require('./models');
const PORT = process.env.PORT || 8000;

// Routes
const Routes = require('./routes/Routes.js');

app.use(express.urlencoded({ extended: true }))
app.use(express.json());
app.use('/user', Routes.UserRoutes);
app.use('/career', Routes.CareerRoutes);

db.sequelize.authenticate().then(async () => {
	console.log('Logging ==> Connected successfully to the database...')
	try {
		const sync_ok = await db.sequelize.sync(); // Database sync (or migrations, call it whatever you want)
		if (sync_ok) {
			app.listen(PORT, () => {
				console.log(`Server listening on http://localhost:${PORT}`)
			});
		} else {
			console.log('Logging ==> Database sync returned an error.')
		}
	} catch (e) {
		console.log(`Logging ==> Exception caught ${e}.`)
	}
}).catch(() => {
	console.log('Logging ==> Could not connect to database.')
});
